#!/bin/sh
# Bash script to email task assignees in Wikimedia Phabricator which have been
# assigned to an open task for more than two years. It takes a CSV input file
# and creates one mbox file, to import into your mail client to send

# This code is licensed under CC0 1.0 Universal:
# https://creativecommons.org/publicdomain/zero/1.0/legalcode
# Author: Andre Klapper <ak-47@gmx.net>, 2020

# Input CSV file must have all lines in this format: username emailaddress 123456
# Input file must have a very last ignored fake line different from previous line

# To manually create the CSV file, run an SQL query on the Wikimedia
# Phabricator database. See https://phabricator.wikimedia.org/T256027

echo "Remember to adjust all parameters at the beginning of this shell script!"

# TODO: Leave as 1 for first email; set to 0 if second email
firstrun=1
# TODO: Adjust input filename
inputfile="sqloutput20230222-at-1677059261.csv"
# TODO: Adjust output filename
outputfile="emailall20230222.mbox"
# TODO: Adjust total number of assignees this time
totalnumberofassignees="144"
# TODO: Adjust current date for email header: Use "date -R" output for this!
currentdate="Date: Wed, 22 Feb 2023 08:00:00 +0200"
# TODO: Adjust current date for email header: Use "date -R" output for this!
firstemaildate="February 22"

i=0
prevemailad=""
taskids=""

rm -f "$outputfile"
while read username emailad taskid; do
  i=$(($i + 1))
  if [ "$i" = "1" ]; then
    taskids="$taskid"
  fi
  if [ "$prevusername" = "$username" ]; then
    taskids="${taskids},${taskid}"
  else
    if [ "$i" != "1" ]; then
#      msgid=`hexdump -n 20 -e '4/4 "%08X"' /dev/random`
      msgid=`openssl rand -hex 20`
      echo "From example@example.com" >> "$outputfile"
      echo "Message-ID: <$msgid.camel@example.com>" >> "$outputfile"
      if [ "$firstrun" = "1" ]; then
        echo "Subject: [ACTION REQUIRED] Your old assigned tasks in Wikimedia Phabricator" >> "$outputfile"
      else
        echo "Subject: [ACTION REQUIRED] You will soon get unassigned from your older tasks in Wikimedia Phabricator" >> "$outputfile"
      fi
      echo "From: example@example.com" >> "$outputfile"
      echo "To: $prevemailad" >> "$outputfile"
      echo "Content-Type: text/plain; charset=\"UTF-8\"" >> "$outputfile"
      echo "Content-Transfer-Encoding: quoted-printable" >> "$outputfile"
      echo "X-Evolution-Identity: c304973451806abb333ba4adf63b68bcdf324256" >> "$outputfile" # needed by GNOME Evolution to match sender account for SMTP verification
      echo "$currentdate" >> "$outputfile"
      echo "MIME-Version: 1.0" >> "$outputfile"
      echo "" >> "$outputfile"
      echo "Hi $prevusername, " >> "$outputfile"
      echo "" >> "$outputfile"
      if [ "$firstrun" != "1" ]; then
        echo "This is a followup to the $firstemaildate email. You will be removed" >> "$outputfile"
        echo "as assignee from tasks assigned for more than two years soon." >> "$outputfile"
      fi
      echo "" >> "$outputfile"
      echo "You receive this message because you are one $totalnumberofassignees people who have" >> "$outputfile"
      echo "open tasks in Wikimedia Phabricator assigned for more than two years:" >> "$outputfile"
      echo "" >> "$outputfile"
      # =3D instead of = because quoted-printable
      echo "https://phabricator.wikimedia.org/maniphest/?ids=3D$taskids#R" >> "$outputfile"
      echo "" >> "$outputfile"
      echo "Are you still realistically working (or planning to work) on them?" >> "$outputfile"
      echo "" >> "$outputfile"
      echo "If you do not plan to work on a task anymore, please remove yourself" >> "$outputfile"
      echo "from the Assignee field (via \"Add Action… 🡒 Assign / Claim\" in the" >> "$outputfile"
      echo "dropdown menu above the comment field), to avoid cookie-licking." >> "$outputfile"
      echo "" >> "$outputfile"
      echo "If a task has been resolved or should not be worked on (\"declined\")," >> "$outputfile"
      echo "then please update its status via \"Add Action… 🡒 Change Status\"." >> "$outputfile"
      echo "" >> "$outputfile"
      echo "If you will work on a task soon and want to stay assigned, please" >> "$outputfile"
      echo "remove yourself as assignee and then assign it again to yourself." >> "$outputfile"
      echo "" >> "$outputfile"
      echo "You can see all your tasks assigned to you at:" >> "$outputfile"
      echo "https://phabricator.wikimedia.org/maniphest/query/assigned/" >> "$outputfile"
      echo "" >> "$outputfile"
      echo "For general info plus help on managing your tasks and work in Phab," >> "$outputfile"
      echo "see: https://www.mediawiki.org/wiki/Bug_management/Assignee_cleanup" >> "$outputfile"
      echo "" >> "$outputfile"
      echo "Please do not reply to this message. For general feedback, use:" >> "$outputfile"
      echo "https://www.mediawiki.org/wiki/Talk:Bug_management/Assignee_cleanup" >> "$outputfile"
      echo "" >> "$outputfile"
      echo "Thanks for your help!" >> "$outputfile"
      echo "andre" >> "$outputfile"
      echo "" >> "$outputfile"
      echo "" >> "$outputfile"
    fi
    prevusername=$username
    prevemailad=$emailad
    taskids="$taskid"
  fi
done < sqloutput.csv
