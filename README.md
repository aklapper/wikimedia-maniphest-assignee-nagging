# wikimedia-maniphest-assignee-nagging

Bash script to email task assignees in Wikimedia Phabricator which have been assigned to an open task for more than two years.

It takes a CSV input file (based on an SQL command you'll have to manually run) and creates an mbox file, to import into your mail client.